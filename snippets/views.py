from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse,JsonResponse
from django.views.decorators.csrf import  csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.parsers import  JSONParser
from rest_framework.views import APIView

from snippets.permissions import IsOwnerOrReadOnly
from snippets.serializers import SnippetSerializer
from snippets.models import  Snippet
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, renderers

from rest_framework import mixins
from rest_framework import generics
from rest_framework import permissions
from snippets.serializers import UserSerializer


from django.contrib.auth.models import User

from rest_framework import viewsets


from rest_framework.decorators import  action
from rest_framework.response import Response
from rest_framework import permissions

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework.response import Response


@api_view(['GET'])
def api_root(request,format=None):
    return Response({
   'users': reverse('user-list',request=request,format=format),
   'snippets':reverse('snippet-list',request=request,format=format)
    })


class SnippetHighlight(generics.GenericAPIView):
    queryset = Snippet.objects.all()
    renderer_classes = [renderers.StaticHTMLRenderer]

    def get(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)








# class SnippetList(APIView):
#
#
#     def get(self,request,form=None):
#         snippts = Snippet.objects.all()
#         serializer = SnippetSerializer(snippts,many=True)
#         return Response(serializer.data)
#
#     def post(self,request,format=None):
#         serializer =SnippetSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data ,status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class SnippetDetail(APIView):
#
#     def get_object(self, pk ):
#         try:
#             return Snippet.objects.get(pk=pk)
#         except Snippet.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#             snippet = self.get_object(pk)
#             serializer = SnippetSerializer(snippet)
#             return Response(serializer.data)
#
#     def put (self , request , pk ,format=None):
#            snippet = self.get_object(pk=pk)
#            serializer= SnippetSerializer(snippet,data=request.data)
#            if serializer.is_valid():
#                serializer.save()
#                return Response(serializer.data)
#            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request,pk,format=None):
#         snippet = self.get_object(pk)
#         snippet.delete()
#         return Response(status.HTTP_204_NO_CONTENT)


###########################Using mixins#########################################

# class SnippetList(mixins.ListModelMixin,
#                   mixins.CreateModelMixin,
#                   generics.GenericAPIView):
#
#         queryset= Snippet.objects.all()
#         serializer_class =SnippetSerializer
#
#         def get(self,request,*args,**kwargs):
#             return self.retrieve(request,*args,**kwargs)
#
#         def post(self, request, *args, **kwargs):
#             return self.create(request, *args, **kwargs)
#
#
#
# class SnippetDetail(mixins.RetrieveModelMixin,
#                     mixins.UpdateModelMixin,
#                     mixins.DestroyModelMixin,
#                     generics.GenericAPIView):
#     queryset = Snippet.objects.all()
#     serializer_class = SnippetSerializer
#
#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
#
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)

######################################Using generic class-based views##################################33

class SnippetList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class SnippetDetail(generics.RetrieveDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer



#########################Sbippets with user##########################################33

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    ializer_class = UserSerializer
    serpermission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def create_perform(self,serializer):
        serializer.save(owner=self.request.user)


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    serpermission_classes = [permissions.IsAuthenticatedOrReadOnly]


# class UserViewSet(viewsets.ReadOnlyModelViewSet):
#     queryset =  User.objects.all()
#     serializer_class = UserSerializer



####################################################################
#
# class SnippetViewSet(viewsets.ModelViewSet):
#     queryset = Snippet.objects.all()
#     serializer_class = SnippetSerializer
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly,
#                           IsOwnerOrReadOnly]
#
#     @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
#     def highlight(self, request, *args, **kwargs):
#         snippet = self.get_object()
#         return Response(snippet.highlighted)
#
#     def perform_create(self, serializer):
#         serializer.save(owner=self.request.user)
